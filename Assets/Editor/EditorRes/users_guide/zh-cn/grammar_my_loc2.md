# 缅文语尾

- 处格/表达动机，因……，旨在……

|格位|含义|翻译建议|关系|
|-|-|-|-|
|[**处**格](https://assets-hk.wikipali.org/pali-handbook/zh-Hans/declension/loc.html)|表达**动机/理由**关系|旨在……<br>因为|[动机 ➡ 动词<br>LOV](https://assets-hk.wikipali.org/pali-handbook/zh-Hans/basic-relation/loc/loc-mot.html)|