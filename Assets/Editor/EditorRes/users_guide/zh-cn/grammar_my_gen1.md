# 缅文语尾

- 名词：属格/表达从属，……的
- 句子结尾：放在首要动词之后

|格位|含义|翻译建议|关系|
|-|-|-|-|
|[**属**格](https://assets-hk.wikipali.org/pali-handbook/zh-Hans/declension/gen.html)|表达**从属关系**|……的|[所有者 ➡ 所属物<br>POS](https://assets-hk.wikipali.org/pali-handbook/zh-Hans/basic-relation/gen/gen-pos.html)|
|——|表达句子结尾：放在首要动词之后|——|——|
