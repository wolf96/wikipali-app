# 缅文语尾

|格位|含义|翻译建议|关系|
|-|-|-|-|
|[**处**格](https://assets-hk.wikipali.org/pali-handbook/zh-Hans/declension/loc.html)|表达**时空背景**关系|在……时<br>在……处|[时空 ➡ 动词<br>LOV](https://assets-hk.wikipali.org/pali-handbook/zh-Hans/basic-relation/loc/loc-lov.html)|
|[宾**格**](https://assets-hk.wikipali.org/pali-handbook/zh-Hans/declension/acc.html#%E8%BF%9B%E9%98%B6%E7%94%A8%E6%B3%95)|表达**时空背景**关系|在……时<br>在……处|[时空 ➡ 动词<br>LOV](https://assets-hk.wikipali.org/pali-handbook/zh-Hans/basic-relation/loc/loc-lov.html)|
|[不变]()|表达**时空背景**关系|在……时<br>在……处|[时空 ➡ 动词<br>LOV](https://assets-hk.wikipali.org/pali-handbook/zh-Hans/basic-relation/loc/loc-lov.html)|