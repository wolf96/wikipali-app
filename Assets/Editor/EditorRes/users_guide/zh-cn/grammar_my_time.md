# 缅文语尾

表示持续一段时间

|格位|含义|翻译建议|关系|
|-|-|-|-|
|[**宾**格](https://assets-hk.wikipali.org/pali-handbook/zh-Hans/declension/acc.html)|表达**持续**一段时间/空间|经过……|[时空连续性<br>STC](https://assets-hk.wikipali.org/pali-handbook/zh-Hans/basic-relation/acc/acc-stc.html)|
|副词|表达**持续**一段时间/空间|经过……|[时空连续性<br>STC](https://assets-hk.wikipali.org/pali-handbook/zh-Hans/basic-relation/acc/acc-stc.html)|
