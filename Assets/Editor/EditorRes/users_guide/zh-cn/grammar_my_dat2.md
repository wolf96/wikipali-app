# 缅文语尾

## 目的

|格位|含义|翻译建议|关系|
|-|-|-|-|
|[**目的**格](https://assets-hk.wikipali.org/pali-handbook/zh-Hans/declension/dat.html)|表达**动作的目的**<br>相当于英语“for”|为了……|[目的 ➡ 动词<br>REC](https://assets-hk.wikipali.org/pali-handbook/zh-Hans/basic-relation/dat/dat-rec.html)|